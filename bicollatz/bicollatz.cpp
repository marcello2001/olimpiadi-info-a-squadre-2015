#include <iostream>

using namespace std;

int bicollatz (int A, int B) {
    int passaggi = 0;
    while (A * B != 1) { // Lavora finchè sono entrambi diversi da 1

        if ((A + B) % 2 == 0) {

            if (A % 2 == 0) {
                A = A / 2;
                B /= 2; // B = B / 2;
            }
            else {
                A = 3 * A + 1;
                B = 3 * B + 1;
            }
        }
        else {
            if (A % 2) {
                A += 3;
            }
            else B += 3;
        }
        passaggi++; //passaggi += 1;
        //cout << A << ',' << B << endl;   //Da usare se qualcosa non va
        if (A > 1000000 || B > 1000000) // Le assunzioni vengono violate, quindi la congettura è falsa
            return -1;
    }
    return passaggi;
}

int main(int argc, char const *argv[]) {
    cout << bicollatz(14,1);
    return 0;
}
