#include <iostream>

using namespace std;

int confronta(int N, char G[], char W[]) {
    int risultato = 0;
    for (int i = 0; i < N; i++) {
        if (G[i] == W[0]) {
            int controllo = 1;
            for (int j = 0; j < N; j++) {
                if ( G[(i+j) % N] != W[j])
                    controllo = 0;
            }
            if (controllo == 1)
                return 1;
        }
    }
    return 0;
}

int main(int argc, char const *argv[]) {
    cout << confronta(7, "abcdefg", "defgabc");
    return 0;
}
